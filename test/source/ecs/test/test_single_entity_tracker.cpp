#include <gmock/gmock.h>

#include "ecs/single_entity_tracker.h"
#include "ecs/test/dummy_ecs.h"

namespace ecs::test
{
namespace
{
TEST(TestSingleEntityTracker, initial_ecs_empty)
{
   ECST ecs;
   SingleEntityTracker<ECST, C1> tracker(ecs);
   EXPECT_THROW(tracker.track(), std::runtime_error);
}

TEST(TestSingleEntityTracker, initial_ecs_has_track)
{
   ECST ecs;
   auto e0 = ecs.addEntity();
   ecs.addComponents<C1, C2, C3>(e0, {}, {}, {}); //Has C1, C2 and C3
   auto e1 = ecs.addEntity();
   ecs.addComponents<C2, C3>(e1, {}, {}); //Does not have C1

   SingleEntityTracker<ECST, C1, C2> tracker(ecs); //Tracks entities with both C1 and C2
   EXPECT_EQ(tracker.track(), e0);
}

TEST(TestSingleEntityTracker, ecs_adds_track)
{
   ECST ecs;
   SingleEntityTracker<ECST, C1, C2> tracker(ecs); //Tracks entities with both C1 and C2

   auto e0 = ecs.addEntity();
   ecs.addComponents<C1, C2, C3>(e0, {}, {}, {}); //Has C1, C2 and C3
   auto e1 = ecs.addEntity();
   ecs.addComponents<C2, C3>(e1, {}, {}); //Does not have C1

   EXPECT_EQ(tracker.track(), e0);
}

TEST(TestSingleEntityTracker, ecs_adds_track_multiple)
{
   ECST ecs;
   SingleEntityTracker<ECST, C1, C2> tracker(ecs); //Tracks entities with both C1 and C2

   auto e0 = ecs.addEntity();
   ecs.addComponents<C1, C2, C3>(e0, {}, {}, {}); //Has C1, C2 and C3
   auto e1 = ecs.addEntity();
   ecs.addComponents<C1, C2, C3>(e1, {}, {}, {});     //Has C1, C2 and C3

   EXPECT_THROW(tracker.track(), std::runtime_error);
}

TEST(TestSingleEntityTracker, ecs_removes_tracked_component)
{
   ECST ecs;
   auto e0 = ecs.addEntity();
   ecs.addComponents<C1, C2, C3>(e0, {}, {}, {}); //Has C1, C2 and C3
   auto e1 = ecs.addEntity();
   ecs.addComponents<C2, C3>(e1, {}, {}); //Does not have C1

   SingleEntityTracker<ECST, C1, C2> tracker(ecs); //Tracks entities with both C1 and C2
   ASSERT_EQ(tracker.track(), e0);
   ecs.removeComponents<C1>(e0);
   EXPECT_THROW(tracker.track(), std::runtime_error);
}

namespace 
{
   struct MockFunctor
   {
      MOCK_CONST_METHOD3(call, void(EntityId id, const C1 &, const C2 &));
   };
}
TEST(TestSingleEntityTracker, foreach)
{
   ECST ecs;
   auto e0 = ecs.addEntity();
   ecs.addComponents<C1, C2>(e0, {}, {}); //Has C1, C2

   auto e1 = ecs.addEntity();
   ecs.addComponents<C2>(e1, {}); //Does not have C1

   const auto &e0c1 = ecs.getComponent<C1>(e0);
   const auto &e0c2 = ecs.getComponent<C2>(e0);

   testing::StrictMock<MockFunctor> functor;

   SingleEntityTracker<ECST, C1, C2> tracker(ecs); //Tracks entities with both C1 and C2
   EXPECT_CALL(functor, call(e0, e0c1, e0c2));
   tracker.foreach([&](EntityId id, const C1 &c1, const C2 &c2) { functor.call(id, c1, c2); });
}
TEST(TestSingleEntityTracker, foreach_multiple_tracks)
{
   ECST ecs;
   auto e0 = ecs.addEntity();
   ecs.addComponents<C1, C2>(e0, {}, {}); //Has C1, C2

   auto e1 = ecs.addEntity();
   ecs.addComponents<C1, C2>(e1, {}, {}); //Has C1, C2

   auto e2 = ecs.addEntity();
   ecs.addComponents<C2>(e2, {}); //Does not have C1

   testing::StrictMock<MockFunctor> functor;

   SingleEntityTracker<ECST, C1, C2> tracker(ecs); //Tracks entities with both C1 and C2
   EXPECT_THROW(tracker.foreach([&](EntityId id, const C1 &c1, const C2 &c2) { functor.call(id, c1, c2); }),
                std::runtime_error);
}
} // namespace
} // namespace ecs
