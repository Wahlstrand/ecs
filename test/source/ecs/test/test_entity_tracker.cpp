#include <gmock/gmock.h>

#include "ecs/entity_tracker.h"
#include "ecs/test/dummy_ecs.h"

namespace ecs::test
{
namespace
{
TEST(TestEntityTracker, initial_ecs_empty)
{
   ECST ecs;
   EntityTracker<ECST, C1> tracker(ecs);
   EXPECT_TRUE(tracker.tracks().empty());
}

TEST(TestEntityTracker, initial_ecs_has_tracks)
{
   ECST ecs;
   auto e0 = ecs.addEntity();
   ecs.addComponents<C1, C2, C3>(e0, {}, {}, {}); //Has C1, C2 and C3
   auto e1 = ecs.addEntity();
   ecs.addComponents<C2, C3>(e1, {}, {}); //Does not have C1

   EntityTracker<ECST, C1, C2> tracker(ecs); //Tracks entities with both C1 and C2
   EXPECT_THAT(tracker.tracks(), testing::UnorderedElementsAre(e0));
}

TEST(TestEntityTracker, ecs_adds_track)
{
   ECST ecs;
   EntityTracker<ECST, C1, C2> tracker(ecs); //Tracks entities with both C1 and C2

   auto e0 = ecs.addEntity();
   ecs.addComponents<C1, C2, C3>(e0, {}, {}, {}); //Has C1, C2 and C3
   auto e1 = ecs.addEntity();
   ecs.addComponents<C2, C3>(e1, {}, {}); //Does not have C1

   EXPECT_THAT(tracker.tracks(), testing::UnorderedElementsAre(e0));
}

TEST(TestEntityTracker, ecs_removes_tracked_component)
{
   ECST ecs;
   auto e0 = ecs.addEntity();
   ecs.addComponents<C1, C2, C3>(e0, {}, {}, {}); //Has C1, C2 and C3
   auto e1 = ecs.addEntity();
   ecs.addComponents<C2, C3>(e1, {}, {}); //Does not have C1

   EntityTracker<ECST, C1, C2> tracker(ecs); //Tracks entities with both C1 and C2
   ASSERT_THAT(tracker.tracks(), testing::UnorderedElementsAre(e0));
   ecs.removeComponents<C1>(e0);
   EXPECT_TRUE(tracker.tracks().empty());
}
TEST(TestEntityTracker, foreach)
{
   ECST ecs;
   auto e0 = ecs.addEntity();
   ecs.addComponents<C1, C2>(e0, {}, {}); //Has C1, C2

   auto e1 = ecs.addEntity();
   ecs.addComponents<C2>(e1, {}); //Does not have C1

   const auto &e0c1 = ecs.getComponent<C1>(e0);
   const auto &e0c2 = ecs.getComponent<C2>(e0);

   struct MockFunctor
   {
      MOCK_CONST_METHOD3(call, void(EntityId id, const C1 &, const C2 &));
   };
   testing::StrictMock<MockFunctor> functor;

   EntityTracker<ECST, C1, C2> tracker(ecs); //Tracks entities with both C1 and C2
   EXPECT_CALL(functor, call(e0, e0c1, e0c2));
   tracker.foreach([&](EntityId id, const C1 &c1, const C2 &c2) { functor.call(id, c1, c2); });
}

} // namespace
} // namespace ecs
