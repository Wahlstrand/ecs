#include <gmock/gmock.h>

#include "ecs/test/dummy_ecs.h"
#include "ecs/test/mock_ecs_listener.h"
#include "ecs/ecs.h"

namespace ecs::test
{
namespace
{
TEST(TestECS, num_entities)
{
   ECST ecs;

   EXPECT_EQ(ecs.numEntities(), 0u);
   ecs.addEntity();
   EXPECT_EQ(ecs.numEntities(), 1u);
   auto e0 = ecs.addEntity();
   EXPECT_EQ(ecs.numEntities(), 2u);
   ecs.removeEntity(e0);
   EXPECT_EQ(ecs.numEntities(), 2u); //Should still be a valid entity, just empty
   ecs.addEntity();
   EXPECT_EQ(ecs.numEntities(), 2u); //Reuse previous entity, no increase
   ecs.addEntity();
   EXPECT_EQ(ecs.numEntities(), 3u);
}

TEST(TestECS, reuse_first_available_idx)
{
   //This is done because we want the used entities
   //to be as dense as possible; i.e.
   // [0 1 2 3 4 X X]
   // rather than
   // [0 X X 3 4 5 6]
   {
      ECST ecs;

      ecs.addEntity();
      auto e1 = ecs.addEntity();
      auto e2 = ecs.addEntity();
      ecs.addEntity();

      ecs.removeEntity(e1);
      ecs.removeEntity(e2);

      EXPECT_EQ(ecs.addEntity(), e1);
   }

   {
      ECST ecs;

      ecs.addEntity();
      auto e1 = ecs.addEntity();
      auto e2 = ecs.addEntity();
      ecs.addEntity();

      ecs.removeEntity(e2);
      ecs.removeEntity(e1);

      EXPECT_EQ(ecs.addEntity(), e1);
   }
}

TEST(TestECS, has_components_single)
{
   ECST ecs;

   auto e0 = ecs.addEntity();
   ecs.addComponents<C1>(e0, {});
   EXPECT_TRUE(ecs.hasComponents<C1>(e0));
   EXPECT_FALSE((ecs.hasComponents<C2>(e0)));
   EXPECT_FALSE((ecs.hasComponents<C1, C3>(e0)));
}
TEST(TestECS, has_components_multiple)
{
   ECST ecs;

   C1 c1;
   C2 c2;
   auto e0 = ecs.addEntity();
   ecs.addComponents<C1, C2>(e0, c1, c2);

   EXPECT_TRUE(ecs.hasComponents<C1>(e0));
   EXPECT_TRUE(ecs.hasComponents<C2>(e0));
   EXPECT_TRUE((ecs.hasComponents<C1, C2>(e0)));
   EXPECT_FALSE((ecs.hasComponents<C1, C3>(e0)));
}
TEST(TestECS, get_component)
{
   ECST ecs;

   C1 c1{.c = 3.14f};
   C2 c2{.c = 61};
   auto e0 = ecs.addEntity();
   ecs.addComponents<C1, C2>(e0, c1, c2);

   EXPECT_EQ(c1, ecs.getComponent<C1>(e0));
   EXPECT_EQ(c2, ecs.getComponent<C2>(e0));
}
TEST(TestECS, remove_component_single)
{
   ECST ecs;

   auto e0 = ecs.addEntity();
   ecs.addComponents<C1>(e0, {});
   ASSERT_TRUE(ecs.hasComponents<C1>(e0));

   ecs.removeComponents<C1>(e0);
   EXPECT_FALSE(ecs.hasComponents<C1>(e0));
}

TEST(TestECS, remove_component_multiple)
{
   ECST ecs;

   auto e0 = ecs.addEntity();
   ecs.addComponents<C1, C2, C3>(e0, {}, {}, {});
   ASSERT_TRUE((ecs.hasComponents<C1, C2, C3>(e0)));

   ecs.removeComponents<C1, C2>(e0);
   EXPECT_FALSE(ecs.hasComponents<C1>(e0));
   EXPECT_FALSE(ecs.hasComponents<C2>(e0));
   EXPECT_TRUE(ecs.hasComponents<C3>(e0));
}

TEST(TestECS, notify_on_add)
{
   ECST ecs;
   MockECSListener mockListener0;
   MockECSListener mockListener1;
   ecs.addListener(&mockListener0);
   ecs.addListener(&mockListener1);

   auto e0 = ecs.addEntity();
   EXPECT_CALL(mockListener0, notifyChanged(e0));
   EXPECT_CALL(mockListener1, notifyChanged(e0));
   ecs.addComponents<C1>(e0, {});
}

TEST(TestECS, dont_notify_removed_listener_on_add)
{
    ECST ecs;
    MockECSListener mockListener0;
    MockECSListener mockListener1;
    ecs.addListener(&mockListener0);
    ecs.addListener(&mockListener1);

    ecs.removeListener(&mockListener1);

    auto e0 = ecs.addEntity();
    EXPECT_CALL(mockListener0, notifyChanged(e0));
    EXPECT_CALL(mockListener1, notifyChanged(e0)).Times(0);
    ecs.addComponents<C1>(e0, {});
}

TEST(TestECS, notify_on_remove)
{
   ECST ecs;
   auto e0 = ecs.addEntity();
   ecs.addComponents<C1>(e0, {});

   MockECSListener mockListener0;
   MockECSListener mockListener1;
   ecs.addListener(&mockListener0);
   ecs.addListener(&mockListener1);

   EXPECT_CALL(mockListener0, notifyChanged(e0));
   EXPECT_CALL(mockListener1, notifyChanged(e0));
   ecs.removeComponents<C1>(e0);
}

TEST(TestECS, dont_notify_removed_listener_on_remove)
{
    ECST ecs;
    auto e0 = ecs.addEntity();
    ecs.addComponents<C1>(e0, {});

    MockECSListener mockListener0;
    MockECSListener mockListener1;
    ecs.addListener(&mockListener0);
    ecs.addListener(&mockListener1);
    ecs.removeListener(&mockListener1);

    EXPECT_CALL(mockListener0, notifyChanged(e0));
    EXPECT_CALL(mockListener1, notifyChanged(e0)).Times(0);
    ecs.removeComponents<C1>(e0);
}


} // namespace
} // namespace ecs::test