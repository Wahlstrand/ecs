#pragma once

#include "ecs/ecs.h"

namespace ecs::test
{
struct C1
{
   float c = 1.0;
};
struct C2
{
   int c = 0;
};
struct C3
{
   char c = 'a';
};

inline bool operator==(const C1& lhs, const C1& rhs)
{
   return lhs.c == rhs.c;
}
inline bool operator==(const C2& lhs, const C2& rhs)
{
   return lhs.c == rhs.c;
}
inline bool operator==(const C3& lhs, const C3& rhs)
{
   return lhs.c == rhs.c;
}

typedef ECS<C1, C2, C3> ECST;
}