#pragma once

#include <vector>
#include "ecs/ecs_fwd.h"
#include "ecs/entity_tracker.h"

namespace ecs
{
template <typename ECST, typename ... TrackedComponentsT>
class SingleEntityTracker
{
public:
    SingleEntityTracker(ECST &ecs);

    //
    EntityId track() const;

    //Callable with entity id + each tracked component: 
    //  func(id, const C1&, const C2&, ....)
    template <typename FuncT>
    void foreach(const FuncT &) const;

private:
    EntityTracker<ECST, TrackedComponentsT...> m_inner;

    void validate() const;
};

} // namespace ecs

#include "ecs/detail/single_entity_tracker.hpp"
