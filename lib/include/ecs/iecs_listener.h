#pragma once

#include "ecs/ecs_fwd.h"

namespace ecs
{
class IECSListener
{
public:
    virtual ~IECSListener(){};
    virtual void notifyChanged(EntityId id) = 0;
};
} // namespace ecs
