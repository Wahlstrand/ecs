#pragma once

namespace ecs
{
class ISystem
{
public:
    virtual ~ISystem() {};
    virtual void update() = 0;
};
} // namespace ecs
