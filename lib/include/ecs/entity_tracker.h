#pragma once

#include <vector>
#include "ecs/ecs_fwd.h"
#include "ecs/iecs_listener.h"

namespace ecs
{
template <typename ECST, typename... TrackedComponentsT>
class EntityTracker : public IECSListener
{
public:
    EntityTracker(ECST &ecs);
    ~EntityTracker();

    //IECSListener
    void notifyChanged(EntityId id) final;

    //
    const std::vector<EntityId> &tracks() const;

    //Callable with entity id + each tracked component: 
    //  func(id, const C1&, const C2&, ....)
    template <typename FuncT>
    void foreach (const FuncT &) const;

private:
    ECST &m_ecs;
    std::vector<EntityId> m_tracks;

    void init();
};

} // namespace ecs

#include "ecs/detail/entity_tracker.hpp"
