#pragma once

namespace ecs
{
template <typename...>
struct index_of;
template <typename T, typename... R>
struct index_of<T, T, R...> : std::integral_constant<size_t, 0> {};
template <typename T, typename F, typename... R>
struct index_of<T, F, R...> : std::integral_constant<size_t, 1 + index_of<T, R...>::value> {};
} // namespace ecs