#pragma once

#include <vector>
#include <array>
#include <tuple>
#include <queue>
#include <limits>
#include "ecs/ecs_fwd.h"
#include "ecs/iecs_listener_fwd.h"

namespace ecs
{
template <typename... AllCompsT>
class ECS
{
public:
    EntityId addEntity();
    void removeEntity(EntityId id);
    
    size_t numEntities() const; //Includes unused ones which are effectively empty entities.

    template <typename FirstCompT, typename SecondCompT, typename... RestT>
    void addComponents(EntityId entityId, FirstCompT first, SecondCompT second, RestT... rest);
    template <typename LastCompT>
    void addComponents(EntityId entityId, LastCompT last);

    template <typename FirstCompT, typename SecondCompT, typename... RestT>
    void removeComponents(EntityId entityId);
    template <typename LastCompT>
    void removeComponents(EntityId entityId);

    template <typename FirstCompT, typename SecondCompT, typename... RestT>
    bool hasComponents(EntityId entityId);
    template <typename LastCompT>
    bool hasComponents(EntityId entityId);

    template <typename CompT>
    CompT& getComponent(EntityId entityId);

    void addListener(IECSListener* pListener);
    void removeListener(IECSListener* pListener);

private:
    typedef std::priority_queue<
        size_t,
        std::vector<size_t>,
        std::greater<size_t>> IdxQueue;
    struct Entity
    {
        static constexpr size_t noComponent = std::numeric_limits<size_t>::max();
        Entity();
        std::array<size_t, sizeof...(AllCompsT)> m_components;
    };
    struct Entities
    {
        std::vector<Entity> m_pool;   //Entity pool. Entities marked as unused should have no components.
        IdxQueue m_unused;            //Unused indices in entity pool. Top is the lowest free entity id
    };
    Entities m_entities;

    struct Components
    {
        std::tuple<std::vector<AllCompsT>...> m_pools;       //Component pools
        std::array<IdxQueue, sizeof...(AllCompsT)> m_unused; //Unused indices in each component pool. Top is the lowest free entity id
    };
    Components m_components;

    std::vector<IECSListener *> m_listeners;

    template <typename ComponentT>
    void addComponent(EntityId id, ComponentT component);

    template <typename ComponentT>
    void removeComponent(EntityId id);

    void notify(EntityId id);
};

} // namespace ecs

#include "ecs/detail/ecs.hpp"
