#include "ecs/ecs.h"

namespace ecs
{
template <typename ECST, typename... TrackedComponentsT>
EntityTracker<ECST, TrackedComponentsT...>::EntityTracker(ECST &ecs)
    : m_ecs(ecs)
{
   m_ecs.addListener(this);
   init();
}
template <typename ECST, typename... TrackedComponentsT>
EntityTracker<ECST, TrackedComponentsT...>::~EntityTracker()
{
   m_ecs.removeListener(this);
}

template <typename ECST, typename... TrackedComponentsT>
void EntityTracker<ECST, TrackedComponentsT...>::notifyChanged(EntityId id)
{
   bool hasComponents = m_ecs.template hasComponents<TrackedComponentsT...>(id);
   auto trackItr = std::find(m_tracks.begin(),
                             m_tracks.end(),
                             id);
   if (hasComponents && trackItr == m_tracks.end())
   {
      //Untracked but should be tracked
      m_tracks.push_back(id);
      return;
   }

   if (!hasComponents && trackItr != m_tracks.end())
   {
      //Tracked but should no longer be tracked
      m_tracks.erase(trackItr);
      return;
   }
}
template <typename ECST, typename... TrackedComponentsT>
const std::vector<EntityId> &EntityTracker<ECST, TrackedComponentsT...>::tracks() const
{
   return m_tracks;
}

template <typename ECST, typename... TrackedComponentsT>
template <typename FuncT>
void EntityTracker<ECST, TrackedComponentsT...>::foreach (const FuncT & func) const
{
   for(const auto id : m_tracks)
   {
      func(id, m_ecs.template getComponent<TrackedComponentsT>(id)...);
   }
}

template <typename ECST, typename... TrackedComponentsT>
void EntityTracker<ECST, TrackedComponentsT...>::init()
{
   size_t nEntities = m_ecs.numEntities();
   for(EntityId id = 0; id < nEntities; id++)
   {
      if(m_ecs.template hasComponents<TrackedComponentsT...>(id))
      {
         m_tracks.push_back(id);
      }
   }
}

} // namespace ecs
