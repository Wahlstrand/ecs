#include "ecs/ecs.h"

namespace ecs
{
template <typename ECST, typename... TrackedComponentsT>
SingleEntityTracker<ECST, TrackedComponentsT...>::SingleEntityTracker(ECST &ecs)
    : m_inner(ecs)
{
}

template <typename ECST, typename... TrackedComponentsT>
EntityId SingleEntityTracker<ECST, TrackedComponentsT...>::track() const
{
   validate();
   return *m_inner.tracks().begin();
}
template <typename ECST, typename... TrackedComponentsT>
template <typename FuncT>
void SingleEntityTracker<ECST, TrackedComponentsT...>::foreach (const FuncT & func) const
{
   validate();
   m_inner.foreach(func);
}
template <typename ECST, typename... TrackedComponentsT>
void SingleEntityTracker<ECST, TrackedComponentsT...>::validate() const
{
   if(m_inner.tracks().size() != 1)
   {
      throw std::runtime_error("Expected single track.");
   }

}
} // namespace ecs
