#include <algorithm>
#include <iostream>
#include "ecs/index.h"
#include "ecs/iecs_listener.h"

namespace ecs
{
template <typename... AllCompsT>
ECS<AllCompsT...>::Entity::Entity()
{
   m_components.fill(std::numeric_limits<EntityId>::max());
}

template <typename... AllCompsT>
EntityId ECS<AllCompsT...>::addEntity()
{
   if (!m_entities.m_unused.empty())
   {
      //Re-use
      EntityId id = m_entities.m_unused.top();
      m_entities.m_unused.pop();
      return id;
   }
   //Add new entity
   m_entities.m_pool.emplace_back();
   return m_entities.m_pool.size() - 1;
}

template <typename... AllCompsT>
void ECS<AllCompsT...>::removeEntity(EntityId id)
{
   //Remove all components
   removeComponents<AllCompsT...>(id);

   //Mark for reuse
   m_entities.m_unused.push(id);
}

template <typename... AllCompsT>
size_t ECS<AllCompsT...>::numEntities() const
{
   return m_entities.m_pool.size();
}

template <typename... AllCompsT>
template <typename FirstCompT, typename SecondCompT, typename... RestT>
void ECS<AllCompsT...>::addComponents(EntityId entityId, FirstCompT first, SecondCompT second, RestT... rest)
{
   addComponent<FirstCompT>(entityId, first);
   addComponents<SecondCompT, RestT...>(entityId, second, rest...);
}
template <typename... AllCompsT>
template <typename LastCompT>
void ECS<AllCompsT...>::addComponents(EntityId entityId, LastCompT last)
{
   //This will be called once the other addComponents method is done "unwinding"
   addComponent<LastCompT>(entityId, std::forward<LastCompT>(last));
   notify(entityId);
}

template <typename... AllCompsT>
template <typename FirstCompT, typename SecondCompT, typename... RestT>
void ECS<AllCompsT...>::removeComponents(EntityId entityId)
{
   removeComponent<FirstCompT>(entityId);
   removeComponents<SecondCompT, RestT...>(entityId);
}
template <typename... AllCompsT>
template <typename LastCompT>
void ECS<AllCompsT...>::removeComponents(EntityId entityId)
{
   //This will be called once the other removeComponents method is done "unwinding"
   removeComponent<LastCompT>(entityId);
   notify(entityId);
}

template <typename... AllCompsT>
template <typename FirstCompT, typename SecondCompT, typename... RestT>
bool ECS<AllCompsT...>::hasComponents(EntityId entityId)
{
   return hasComponents<FirstCompT>(entityId) && hasComponents<SecondCompT, RestT...>(entityId);
}
template <typename... AllCompsT>
template <typename LastCompT>
bool ECS<AllCompsT...>::hasComponents(EntityId entityId)
{
   auto& entity = m_entities.m_pool[entityId];
   return entity.m_components[index_of<LastCompT, AllCompsT...>::value] != Entity::noComponent;
}

template <typename... AllCompsT>
template <typename CompT>
CompT& ECS<AllCompsT...>::getComponent(EntityId entityId)
{
   auto& comps = std::get<std::vector<CompT>>(m_components.m_pools);
   auto const& entity = m_entities.m_pool[entityId];
   auto const cIdx = entity.m_components[index_of<CompT, AllCompsT...>::value];
   return comps[cIdx];
}

template <typename... AllCompsT>
void ECS<AllCompsT...>::addListener(IECSListener *pListener)
{
   m_listeners.push_back(pListener);
}
template <typename... AllCompsT>
void ECS<AllCompsT...>::removeListener(IECSListener *pListener)
{
    m_listeners.erase(
        std::remove(
            m_listeners.begin(),
            m_listeners.end(),
            pListener),
        m_listeners.end());
}

template <typename... AllCompsT>
template <typename CompT>
void ECS<AllCompsT...>::addComponent(EntityId entityId, CompT comp)
{
   auto &pool = std::get<std::vector<CompT>>(m_components.m_pools);
   auto &unused = m_components.m_unused[index_of<CompT, AllCompsT...>::value];

   size_t cIdx = 0;
   if (!unused.empty())
   {
      cIdx = unused.top();
      unused.pop();
      pool[cIdx] = std::forward<CompT>(comp);
   }
   else
   {
      cIdx = pool.size();
      pool.emplace_back(std::forward<CompT>(comp));
   }

   auto &entity = m_entities.m_pool[entityId];
   entity.m_components[index_of<CompT, AllCompsT...>::value] = cIdx;
}

template <typename... AllCompsT>
template <typename CompT>
void ECS<AllCompsT...>::removeComponent(EntityId entityId)
{
   auto &entity = m_entities.m_pool[entityId];
   size_t cIdx = entity.m_components[index_of<CompT, AllCompsT...>::value];

   if (cIdx == Entity::noComponent)
   {
      //Entity doesn't have this component.
      return;
   }

   //Remove component from entity
   entity.m_components[index_of<CompT, AllCompsT...>::value] = Entity::noComponent;

   //Add component to unused pool
   auto &unused = m_components.m_unused[index_of<CompT, AllCompsT...>::value];
   unused.push(cIdx);
}

template <typename... AllCompsT>
void ECS<AllCompsT...>::notify(EntityId entityId)
{
   for (auto &&pListener : m_listeners)
   {
      pListener->notifyChanged(entityId);
   }
}

} // namespace ecs
