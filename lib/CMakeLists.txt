cmake_minimum_required (VERSION 3.14)

file(GLOB_RECURSE SRC CONFIGURE_DEPENDS "*.h" "*.hpp" "*.cpp")
add_library(ecs ${SRC})
add_library(ecs::lib ALIAS ecs)

source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR} FILES ${SRC})

target_include_directories (ecs PUBLIC include)
target_link_libraries(ecs PRIVATE ecs::flags)